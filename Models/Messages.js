const mongoose = require("mongoose")



const MessageSchema = new mongoose.Schema({
    roomId: {
        type: String,
        require: true
    },
    sender: {
        type: String,
        require: true
    },
    message: {
        type: String,
    },
},
    {
        timestamps: true
    }
)



module.exports = mongoose.model("Message", MessageSchema)