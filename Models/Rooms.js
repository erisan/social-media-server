const mongoose = require("mongoose")



const RoomsSchema = new mongoose.Schema({
    members: {
        type: Array,
        require: true
    }
    
},
    {
        timestamps: true
    }
)



module.exports = mongoose.model("Rooms", RoomsSchema)