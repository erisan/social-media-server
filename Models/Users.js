const mongoose = require("mongoose")



const UserSchema = new mongoose.Schema({
    fullname: {
        type: String,
        require: true
    },
    username: {
        type: String,
        unique: true,
        require: true
    },
    email: {
        type: String,
        unique: true,
        require: true,
        max: 60
    },
    password: {
        type: String,
        require: true,
        max: 60,
        min: 6
    },
    profile: {
        type: String,
        default: ""
    },
    cover: {
        type: String,
        default: ""
    },
    followers: {
        type: Array,
        default: []
    },

    following: {
        type: Array,
        default: []
    },
    isAdmin: {
        type: Boolean,
        default: false
    }
},
{
    timestamps: true
}
)



module.exports  = mongoose.model("User" , UserSchema)