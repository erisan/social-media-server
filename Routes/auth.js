const User = require("../Models/Users")
const bcrypt = require('bcrypt')
const mongoose = require("mongoose")
var jwt = require('jsonwebtoken');
const router = require("express").Router()



router.post("/signup", async (req, res) => {

    const { username, email, password , fullname} = req.body

    const userExist = await User.findOne({ $or: [{ email }, { username }] })
    userExist && res.status(400).json({ msg: "Username or email exist" })

    const hashPassword = await bcrypt.hash(password, 10)

    const user = new User({
        username,
        email,
        fullname,
        password: hashPassword
    })

    try {
        await user.save()
        res.status(200).json({ msg: "User created successfully" })
    } catch (error) {
        console.log(error)
    }


})



router.post("/signin", async (req, res) => {
    try {
        const { email, password } = req.body
        const user = await User.findOne({ email })
        !user && res.status(404).json({ msg: "No user found" })
        const validPassword = await bcrypt.compare(password, user.password)
        !validPassword && res.status(400).json({ msg: "Invalid email or password" })

        const token = jwt.sign(
            { _id: user._id, email },
            process.env.JWT_SECRETE,
            {
                expiresIn: "2000h",
            }
        );
        const { password: d, updatedAt, ...data } = user?._doc

        return res.status(200).json({ msg: "Login successful", data: data, token })

    } catch (error) {
        console.log(error)
        res.status(500).json({ msg: "Internal server error" })
    }


})


module.exports = router