const Messages = require("../Models/Messages")
const router = require("express").Router()

// get

router.post("/", async (req, res) => {
    try {
        const message = new Messages({ roomId: req.body.roomId, sender: req.body.sender, message: req.body.message })
        await message.save()
        res.status(200).json({ msg: "Message sent" })
    } catch (error) {
        console.log(error)
    }
})

router.get("/:roomId", async (req, res) => {
    try {
        const message = await  Messages.find({roomId: req.params.roomId})
        res.status(200).json({ msg: "Success"  , data: message})
    } catch (error) {
        console.log(error)
    }
})



module.exports = router