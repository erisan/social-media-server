const User = require("../Models/Users")
const bcrypt = require('bcrypt')
const mongoose = require("mongoose")
var jwt = require('jsonwebtoken');
const Post = require("../Models/Posts")
const router = require("express").Router()

// get

router.get("/:id", async (req, res) => {
    try {
        const post = await Post.findById(req.params.id)
        if (!post) return res.status(404).json({ msg: "Post not found" });

        res.status(200).json({ msg: "Success", data: post })
    } catch (error) {
        console.log(error)
    }


})

// get timeline

router.get("/feed/all/", async (req, res) => {
    console.log(req.query.userId)
    try {
        const post = await Post.findById(req.query.userId)
        if (!post) return res.status(404).json({ msg: "User doesn't have any post" });

        res.status(200).json({ msg: "Success", data: post })
    } catch (error) {
        console.log(error)
    }


})

// create

router.post("/", async (req, res) => {
    const { desc, img, userId } = req.body
    try {
        const post = new Post({ ...req.body, userId: req.user })
        await post.save()
        res.status(200).json({ msg: "Post created successfully" })
    } catch (error) {
        console.log(error)
    }


})


// update a post

router.put("/:id", async (req, res) => {
    try {
        const post = await Post.findById(req.params.id)
        post.userId !== req.user && res.status(403).json({ msg: "You can only edit your post" })
        await post.updateOne({ $set: req.body })
        res.status(200).json({ msg: "Post updated successfully" })
    } catch (error) {
        console.log(error)
    }


})

// delete a post
router.delete("/:id", async (req, res) => {
    try {
        const post = await Post.findById(req.params.id)
        console.log(post, "dsdd")
        post.userId !== req.user && res.status(403).json({ msg: "You can only delete your post" })
        await post.deleteOne()
        res.status(200).json({ msg: "Post deleted successfully" })
    } catch (error) {
        console.log(error)
    }


})

// like a post
router.put("/:id/like", async (req, res) => {
    try {
        console.log(req.params.id, "req.params.id")
        const post = await Post.findOne({ _id: req.body.id })
        if (!post) return res.status(404).json({ msg: "Post not found" });
        if (post.likes.includes(req.body.userId)) {
            await post.updateOne({
                $pull: {
                    likes: [req.body.userId]
                }
            })
            res.status(200).json({ msg: "Post unliked successfully" })
        }
        else {
            await post.updateOne({
                $push: {
                    likes: [req.body.userId]
                }
            })

            res.status(200).json({ msg: "Post liked successfully" })


        }

    } catch (error) {
        console.log(error)
    }

})



module.exports = router