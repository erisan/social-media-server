const Rooms = require("../Models/Rooms")
const router = require("express").Router()

// create rooms
router.post("/", async (req, res) => {
    try {
        const room = new Rooms({
            members  : [req.body.senderId , req.body.receiverId]
        })
        await room.save()
        res.status(200).json({ msg: "Success"})
    } catch (error) {
        console.log(error)
    }

})

// get All rooms
router.get("/", async (req, res) => {
    console.log(req.user, "ppppppppppp")
    try {
         const room = await Rooms.find({
            members :{
                $in:req.user
            }
         }).limit(10)
        res.status(200).json({ msg: "Rooms" , data: room})
    } catch (error) {
        console.log(error)
    }
})


module.exports = router