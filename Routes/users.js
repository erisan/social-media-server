
const router = require("express").Router()
const Users = require("../Models/Users")
const Rooms = require("../Models/Rooms")
const bcrypt = require("bcrypt")



// getUser user
router.get("/me", async (req, res) => {
    console.log("Odebiiiiiiiiiiii")

    try {
        const user = await Users.findById(req.user)
        !user && res.status(403).json({ msg: "No user found" })
        const { password, updatedAt, ...data } = user?._doc
        res.status(200).json({ msg: "Success", data: data })
    } catch (error) {
        console.log(error)
    }

})
// getUser user by id
router.get("/:id", async (req, res) => {
    console.log("Odebiiiiiiiiiiii")
    try {
        const user = await Users.findById(req.params.id)
        !user && res.status(403).json({ msg: "No user found" })
        const { password, updatedAt, ...data } = user?._doc
        res.status(200).json({ msg: "Success", data: data })
    } catch (error) {
        console.log(error)
    }

})

// update user
router.put("/", async (req, res) => {
    try {
        if (req.body.password) {
            req.body.password = await bcrypt.hash(req.body.password, 10)
            await Users.findByIdAndUpdate(req.user, { ...req.body })
            res.status(200).json({ msg: "User updated successfully" })
        }
    } catch (error) {
        console.log(error)
    }

})

//delete user
router.delete("/", async (req, res) => {
    await Users.findByIdAndDelete(req.user)
    res.status(200).json({ msg: "User deleted successfully" })
})


//follow a user
router.put("/follow", async (req, res) => {
    console.log(req.body , "-----------------------")
    try {
        // check if following user exist
        const user = await Users.findById(req.body.userId)
        !user && res.status(400).json({ msg: "User you want to follow does not exist" })
        if(req.body.userId == req.user) return  res.status(400).json({ msg: "You can not follow yourself" })
        // Check if already followed user
        const followingExist = await Users.find({
            _id: req.user,
             following: {
            $in: req.body.userId
        }
        })
       
       if(followingExist.length > 0) return res.status(400).json({ msg: "You followed this user already" })
        // update the user following field - Add to following
        await Users.findByIdAndUpdate(req.user, {
            $push: {
                following: req.body.userId
            }
        })

        // Update the user you are following followers field - Add to followers
        await Users.findByIdAndUpdate(req.body.userId, {
            $push: {
                followers: req.user
            }
        })

        // Create room if not exist

        const roomExist = await Rooms.findOne({
            $and:[
                {
                    members:{
                        $in: req.user
                    },
                    members:{
                        $in: req.body.userId
                    }
                }
            ]
          
        })
    
        console.log(roomExist, "kkkkkkkkkkkkkkkkkkkkkkkk" , req.body.userId)

        if(!roomExist){
            const room = new Rooms({
                members:
                    [req.user , req.body.userId]
                
            })

            console.log([req.user , req.body.userId] , "req.user , req.body.userId")
            await room.save()
        }

        return res.status(200).json({ msg: "Followed user successfully" })

    } catch (error) {
        console.log(error)
    }
})



//follow a user
router.put("/unfollow", async (req, res) => {
    try {
        // check if following user exist
        const user = await Users.findById(req.body.userId)
        !user && res.status(400).json({ msg: "User you want to unfollow does not exist" })
        req.body.userId == req.user && res.status(400).json({ msg: "You can not unfollow yourself" })

        // Remove the user you want to unfollow from following
        await Users.findByIdAndUpdate(req.user, {
            $pull: {
                following: [req.body.userId]
            }
        })


        // Remove followers - since they are unfollowing this user
        await Users.findByIdAndUpdate(req.body.userId, {
            $pull: {
                followers: [req.user]
            }
        })


        return res.status(200).json({ msg: "Unfollowed user successfully" })

    } catch (error) {
        console.log(error)
    }
})


//who to follow 
router.get("/who/suggest", async (req, res) => {
    try {
        // check if following user exist
        const users = await Users.find({
            followers:{
                $nin:[req.user]
            }
        }, "-password -updatedAt").limit(req.params?.limit || 10)
        
        return res.status(200).json({data: users})

    } catch (error) {
        return res.status(500).json({msg: "Internal server error"})
    }
})



module.exports = router