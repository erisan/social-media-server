
require("custom-env").env();
const jwt = require("jsonwebtoken")

const withAuthentation = (req, res, next) => {
    const secrete = process.env
    const token = req.headers["Authorization"] || req.headers["authorization"] || req.body["token"]

    if (!token) {
        return res.status(403).json({ msg: `Authorization header is missing` })
    }
    try {
        const currentUser = jwt.verify(token, secrete.JWT_SECRETE);
        req.user = currentUser?._id
        next()
    } catch (error) {
        return res.status(403).json({ msg: `Invalid token` })
    }



}

module.exports = withAuthentation