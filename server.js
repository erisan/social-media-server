
const express = require("express")
const app = express()
const helmet = require("helmet");
const port = process.env.port || 8030
var morgan = require('morgan')
const mongoose = require('mongoose');
const customEnv = require('custom-env').env()
const authRoute = require("./Routes/auth")
const userRoute = require("./Routes/users")
const postRoute = require("./Routes/posts")
const messageRouter = require("./Routes/messages")
const roomRouter = require("./Routes/rooms")
const withAuthentation = require("./middlewares/authentication")
const cors = require('cors')
customEnv.env()


app.use(cors())
app.use(express.json())
app.use(helmet());
app.use(morgan("common"))


app.use("/api/auth", authRoute)

app.use("/api/post", withAuthentation, postRoute)
app.use("/api/rooms", withAuthentation, roomRouter)
app.use("/api/messages", withAuthentation, messageRouter)
app.use("/api/user", withAuthentation, userRoute)


mongoose.connect(process.env.DB_URL, { autoIndex: true }, (error) => {
  console.log(error, "this is error")
});





app.get("/", (req, res) => {
  mongoose.connection.db.dropCollection("users")
  mongoose.connection.db.dropCollection("rooms")
  mongoose.connection.db.dropCollection("messages")
  mongoose.connection.db.dropCollection("conversations")
  res.send("Welcome to home")
})


app.listen(port, () => {
  console.log(`Server running on ${port}`)
}) 